<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * Bài test 1 : Công ty đang cần một trang nội bộ để bộ phận Sale-Admin có thể tạo và quản lý các hợp đồng dịch vụ hosting cho khách hàng
 * y/c :  
 * 
 * 	- Cần thêm các trường tương ứng vào table hosting trong database để phù hợp với các trường của hợp đồng trong trang test_1/index
 * 	- Trang danh sách func index() - liệt kê tất cả các hợp đồng hosting đang được quản lý
 * 		+ Liệt kê tất cả hợp đồng
 * 		+ cho phép delete hợp đồng - khi click có hiển thị modal y/c sale-admin xác nhận xóa hợp đồng
 * 	- Trang tạo mới func create() - khởi tạo một hợp đồng dịch vụ hosting
 * 	- Trang cập nhật func update() - Cập nhật hợp đồng
 * 	- Trang tạo mới và cập nhật dữ liệu yêu cầu : 
 * 	
 * 		+ Hiển thị tương tự các gói dịch vụ hosting tương tự như hình hosting_packages.png ở folder src/image/hosting_packages.png
 * 		+ field gói dịch vụ là một giá trị trong ( là key của tập config hosting ở file config/hosting.php )
 * 			* h5
 *			* h9
 *			* h13
 *			* h16
 *			* h20
 *			* h25
 *			* h45
 *		+ Y/c sử dụng array từ file hosting/config và sử dụng vòng lặp để  hiển thị ra dạng table tương tự ảnh src/image/hosting_packages.png
 * 	
 * 	- Sử dụng các function tích hợp sẵn ở file model Hosting_m
 * 	- Cần validate dữ liệu trước khi insert|update
 */

class Test_1 extends CI_Controller {

	function __construct() 
	{
		parent::__construct();
		$this->load->model('hosting_m');
		$this->load->config('hosting');
	}

	public function index()
	{
		$hostings = $this->hosting_m->get_all();

		$config = $this->config->item('service', 'packages');
		$data = array(
			'hostings' => $hostings,
			'config' => $config
		);

		$this->load->view('test_1/index', $data);
	}

	public function create()
	{
		$data = array();
		/*
		 * sử dụng $this->input->post() để lấy tất cả dữ liệu được post lên
		 * sử dụng $this->hosting_m->insert($data) để tiến hành insert vào database
		 */
		$this->load->view('test_1/create', $data);
	}

	public function update($id = 0)
	{
		$data = array();
		/*
		 * sử dụng model hosting_m để lấy dữ liệu dựa trên ID , vd : $this->hosting_m->get($id);
		 * sử dụng $this->input->post() để lấy tất cả dữ liệu được post lên
		 * sử dụng $this->hosting_m->insert($data) để tiến hành insert vào database
		 */
		$post = $this->input->post();
		$this->load->view('test_1/update', $data);
	}
}