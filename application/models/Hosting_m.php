<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); 

class Hosting_m extends MY_Model {

	public $primary_key = 'id';
	public $_table = 'hosting';
}
/* End of file Hosting_m.php */
/* Location: ./application/models/Hosting_m.php */