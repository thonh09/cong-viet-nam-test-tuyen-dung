<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="utf-8">
        <title>Trang Cập nhật hợp đồng HOSTING</title>
        <link rel="stylesheet" href="<?php echo base_url('template/bootstrap/css/bootstrap.min.css') ;?>">
        <script type="text/javascript" src="<?php echo base_url('template/bootstrap/js/bootstrap.min.js') ;?>"></script>
    </head>

    <body>

        <div id="container">

            <div id="body" class="row">
                <div class="container">

                    <table border="1" cellpadding="2" cellspacing="1" class="table table-responsive table-bordered table-hover col-6">
                        <caption>Chi tiết dịch vụ</caption>
                        <thead>
                            <tr>
                                <th></th>
                                <th class="text-center">
                                    <label>
                                    <input type="radio" name="hosting_service_package" value="h5gb">
                                    <br>Cá nhân</label>
                                </th>
                                <th class="text-center">
                                    <label class="">
                                    <input type="radio" name="hosting_service_package" value="h9gb">
                                    <br>Doanh nghiệp nhỏ</label>
                                </th>
                                <th class="text-center">
                                    <label class="">
                                    <input type="radio" name="hosting_service_package" value="h13gb">
                                    <br>Doanh nghiệp vừa &amp; nhỏ</label>
                                </th>
                                <th class="text-center">
                                    <label class="">
                                    <input type="radio" name="hosting_service_package" value="h16gb">
                                    <br>Doanh nghiệp vừa</label>
                                </th>
                                <th class="text-center">
                                    <label class="">
                                    <input type="radio" name="hosting_service_package" value="h20gb">
                                    <br>Doanh nghiệp lớn</label>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Giá trị /tháng</td>
                                <td class="text-center">290.000 /tháng</td>
                                <td class="text-center">410.000 /tháng</td>
                                <td class="text-center">510.000 /tháng</td>
                                <td class="text-center">610.000 /tháng</td>
                                <td class="text-center">710.000 /tháng</td>
                            </tr>
                            <tr>
                                <td>Giá trị /năm</td>
                                <td class="text-center">3.480.000 /năm</td>
                                <td class="text-center">4.920.000 /năm</td>
                                <td class="text-center">6.120.000 /năm</td>
                                <td class="text-center">7.320.000 /năm</td>
                                <td class="text-center">8.520.000 /năm</td>
                            </tr>
                            <tr>
                                <td>Dung lượng lưu trữ (1GB = 1000MB)</td>
                                <td class="text-center">5 GB</td>
                                <td class="text-center">9 GB</td>
                                <td class="text-center">13 GB</td>
                                <td class="text-center">16 GB</td>
                                <td class="text-center">20 GB</td>
                            </tr>
                            <tr>
                                <td>Lưu lượng truy cập</td>
                                <td class="text-center">170.000 Lượt/tháng</td>
                                <td class="text-center">320.000 Lượt/tháng</td>
                                <td class="text-center">Không giới hạn</td>
                                <td class="text-center">Không giới hạn</td>
                                <td class="text-center">Không giới hạn</td>
                            </tr>
                            <tr>
                                <td>Subdomain</td>
                                <td class="text-center">10</td>
                                <td class="text-center">40</td>
                                <td class="text-center">60</td>
                                <td class="text-center">80</td>
                                <td class="text-center">100</td>
                            </tr>
                            <tr>
                                <td>Addon domain</td>
                                <td class="text-center">1</td>
                                <td class="text-center">2</td>
                                <td class="text-center">2</td>
                                <td class="text-center">3</td>
                                <td class="text-center">3</td>
                            </tr>
                            <tr>
                                <td>MySQL</td>
                                <td class="text-center">3</td>
                                <td class="text-center">5</td>
                                <td class="text-center">6</td>
                                <td class="text-center">7</td>
                                <td class="text-center">7</td>
                            </tr>
                            <tr>
                                <td>Sao lưu dữ liệu</td>
                                <td class="text-center">Hàng tuần</td>
                                <td class="text-center">Hàng tuần</td>
                                <td class="text-center">Hàng ngày</td>
                                <td class="text-center">Hàng ngày</td>
                                <td class="text-center">Hàng ngày</td>
                            </tr>
                            <tr>
                                <td>Tối ưu chạy quảng cáo GG-FB</td>
                                <td class="text-center">Có</i></td>
                                <td class="text-center">Có</i></td>
                                <td class="text-center">Có</i></td>
                                <td class="text-center">Có</i></td>
                                <td class="text-center">Có</i></td>
                            </tr>
                            <tr>
                                <td>Miễn phí SSL</td>
                                <td class="text-center">Có</i></td>
                                <td class="text-center">Có</i></td>
                                <td class="text-center">Có</i></td>
                                <td class="text-center">Có</i></td>
                                <td class="text-center">Có</i></td>
                            </tr>
                            <tr>
                                <td>Miễn phí tên miền năm đầu</td>
                                <td class="text-center">--</td>
                                <td class="text-center">--</td>
                                <td class="text-center">Có</i></td>
                                <td class="text-center">Có</i></td>
                                <td class="text-center">Có</i></td>
                            </tr>
                            <tr>
                                <td>E-mail/WebMail</td>
                                <td class="text-center">--</td>
                                <td class="text-center">--</td>
                                <td class="text-center">--</td>
                                <td class="text-center">--</td>
                                <td class="text-center">--</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </body>

    </html>