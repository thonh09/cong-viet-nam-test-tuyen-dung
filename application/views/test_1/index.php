<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/* Dữ liệu từ controller được truyền xuống view thông qua var $data từ controllẻr 
* Được gọi tương tự như sau, sau khi hoàn thành y/c xóa 2 line 7->8
*/
// var_dump($config);
// var_dump($hostings);

?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Trang danh sách hợp đồng HOSTING</title>

	<link rel="stylesheet" href="<?php echo base_url('template/bootstrap/css/bootstrap.min.css') ;?>">
	<script type="text/javascript" src="<?php echo base_url('template/bootstrap/js/bootstrap.min.js') ;?>"></script>

</head>
<body>

	<div id="container">
		<div id="body" class="row">
			<!-- Main content -->
			<div class="container">
				<div class="col-xs-12">
					<div class="box">
						<div class="box-header">
							<h3 class="box-title">Danh sách hđ hosting</h3>

							<div class="box-tools">
								<div class="input-group input-group-sm" style="width: 150px;">
									<div class="input-group-btn float-right">
										<button type="button" class="btn btn-default">Thêm mới</button>
									</div><br/><br/>
								</div>
							</div>
						</div>
						<!-- /.box-header -->
						<div class="box-body table-responsive no-padding">
							<table class="table table-hover">
								<tr>
									<th>ID</th>
									<th>Mã hợp đồng</th>
									<th>Ngày tạo</th>
									<th>Gói hosting</th>
									<th>Actions</th>
								</tr>
								<tr>
									<td>183</td>
									<td>HOSITNG01</td>
									<td>09-09-2018</td>
									<td><span class="label label-success">Cá nhân</span></td>
									<td>
										<button type="button" class="btn btn-danger">Xóa</button>
										<button type="button" class="btn btn-default">Cập nhật</button>
									</td>
								</tr>
							</table>
						</div>
						<!-- /.box-body -->
					</div>
					<!-- /.box -->
				</div>
			</div>
		</div>
	</div>

</body>
</html>